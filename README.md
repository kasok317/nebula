# Nebula

#Install Binary

wget https://github.com/slackhq/nebula/releases/download/v1.3.0/nebula-linux-amd64.tar.gz

#Unzip into OPT

mkdir /opt/nebula

tar -xf nebula-linux-amd64.tar.gz -C /opt/nebula

#Create new host

./nebula-cert sign -name "HOST" -ip "172.16.X.X/24" -groups "home" # currently used .9

#Copy files to new host

ssh root@"HOST" "mkdir /opt/nebula

sudo rsync "HOST".* root@"HOST":/opt/nebula

sudo rsync ca.crt root@"HOST":/opt/nebula

sudo rsync nebula root@"HOST":/opt/nebula

sudo rsync host_config.yaml root@"HOST":/opt/nebula

sudo rsync /etc/systemd/system/nebula.service root@"HOST":/etc/systemd/system
